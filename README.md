# Node 16 Issue

Purpose of this project is to show node16 issue when building using gitlab runner.

Example pipeline can be seen here:
https://gitlab.com/michalpiasecki/node-16-issue/-/pipelines/406336326

To build node16 service locally, checkout the code and run:

```
docker build --no-cache --file node16/Dockerfile .
[+] Building 211.1s (11/11) FINISHED
 => [internal] load build definition from Dockerfile                                                                     0.0s
 => => transferring dockerfile: 37B                                                                                      0.0s
 => [internal] load .dockerignore                                                                                        0.0s
 => => transferring context: 2B                                                                                          0.0s
 => [internal] load metadata for docker.io/library/node:16-alpine                                                        1.3s
 => [internal] load build context                                                                                        0.0s
 => => transferring context: 72B                                                                                         0.0s
 => [1/6] FROM docker.io/library/node:16-alpine@sha256:3bca55259ada636e5fee8f2836aba7fa01fed7afd0652e12773ad44af95868b9  0.0s
 => CACHED [2/6] WORKDIR /usr/src/app                                                                                    0.0s
 => [3/6] RUN apk add --no-cache bash python3 make g++                                                                   6.7s
 => [4/6] RUN npm install -g http-server                                                                                 7.6s
 => [5/6] COPY package*.json ./                                                                                          0.0s
 => [6/6] RUN npm install                                                                                              178.8s
 => exporting to image                                                                                                  16.5s
 => => exporting layers                                                                                                 16.5s
 => => writing image sha256:9966b59a96b90f91501abfd38df818e7cf3f69f9d6c4055c8ccc81d2115e3ce7                             0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
```

Docker version I am  using locally is the same as in .gitlab-ci.yml

```
docker --version
Docker version 20.10.7, build f0df350
```
